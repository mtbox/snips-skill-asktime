#!/usr/bin/env python2
#import os.path
from hermes_python.hermes import Hermes
from datetime import datetime
from pytz import timezone

SNIPS_CONFIG_PATH = '/etc/snips.toml'
mqttServer = 'localhost'
mqttPort = 1883
MQTT_ADDR = "{}:{}".format(mqttServer, str(mqttPort))

#def loadConfigs():
#	global mqttServer, mqttPort
#
#	if os.path.isfile(SNIPS_CONFIG_PATH):
#		with open(SNIPS_CONFIG_PATH) as confFile:
#			configs = pytoml.load(confFile)
#			if 'mqtt' in configs['snips-common']:
#				if ':' in configs['snips-common']['mqtt']:
#					mqttServer = configs['snips-common']['mqtt'].split(':')[0]
#					mqttPort = int(configs['snips-common']['mqtt'].split(':')[1])
#				elif '@' in configs['snips-common']['mqtt']:
#					mqttServer = configs['snips-common']['mqtt'].split('@')[0]
#					mqttPort = int(configs['snips-common']['mqtt'].split('@')[1])
#	else:
#		print('Snips configs not found')
#
#	MQTT_ADDR = "{}:{}".format(mqttServer, str(mqttPort))


def verbalise_hour(i):
	if i == 0:
		return "minuit"
	elif i == 1:
		return "une heure"
	elif i == 12:
		return "midi"
	elif i == 21:
		return "vingt et une heures"
	else:
		return "{0} heures".format(str(i))

def verbalise_minute(i):
	if i == 0:
		return ""
	elif i == 1:
		return "une"
	elif i == 21:
		return "vingt et une"
	elif i == 31:
		return "trente et une"
	elif i == 41:
		return "quarante et une"
	elif i == 51:
		return "cinquante et une"
	else:
		return "{0}".format(str(i))


def intent_received(hermes, intent_message):

	print()
	print(intent_message.intent.intent_name)
	print ()

	if intent_message.intent.intent_name == 'kit0u:askTime1' and intent_message.intent.probability >= 0.9:

		sentence = 'Il est '
		print(intent_message.intent.intent_name)

		now = datetime.now(timezone('Europe/Paris'))

		sentence += verbalise_hour(now.hour) + " " + verbalise_minute(now.minute)
		print(sentence)

		# hermes.publish_continue_session(intent_message.session_id, sentence, ["Joseph:greetings"])
		hermes.publish_end_session(intent_message.session_id, sentence)

	elif intent_message.intent.intent_name == 'kit0u:greetings':

		hermes.publish_end_session(intent_message.session_id, "De rien!")


# loadConfigs()

with Hermes(MQTT_ADDR) as h:
	h.subscribe_intents(intent_received).start()
